import {
  SET_LOADING,
  GET_CATEGORIES,
  ADD_CATEGORY,
  CATEGORY_ERROR,
  SET_CURRENT,
  SET_CURRENT_EDITING,
  CLEAR_CURRENT,
  CLEAR_CURRENT_EDITING
} from "../actions/types";

const initialState = {
  categories: null,
  current: null,
  currentEditing: null,
  loading: false,
  error: null
};
export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
        loading: false
      };
    case ADD_CATEGORY:
      return {
        ...state,
        categories: [...state.categories, action.payload]
      };
    case CATEGORY_ERROR:
      return {
        ...state,
        error: action.payload
      };
    case SET_CURRENT:
      return {
        ...state,
        current: action.payload
      };
    case SET_CURRENT_EDITING:
      return {
        ...state,
        currentEditing: action.payload
      };
    case CLEAR_CURRENT:
      return {
        ...state,
        current: null
      };
    case CLEAR_CURRENT_EDITING:
      return {
        ...state,
        currentEditing: null
      };
    case SET_LOADING:
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
};
