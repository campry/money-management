import React, {useEffect, useState, useRef} from 'react';
import IconSelect from '../../form-components/icon-select/IconSelect.js'
import './CategoryBuilder.scss'
import M from "materialize-css/dist/js/materialize.min";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {addCategory} from "../../../../actions/categoryAction";

const CategoryBuilder = ({addCategory}) => {

  const categoryName = useRef('');
  const [categoryIcon, setCategoryIcon] = useState('');

  useEffect(() => {
    const htmlElement = document.getElementById('category-builder-modal');
    M.Modal.init(htmlElement, null);
    console.log(categoryName);
  }, []);

  const onIconSelected = icon => {
    setCategoryIcon(icon);
  };

  const onSubmit = e => {
    e.preventDefault();
    if (!categoryName.current.value.length || !categoryIcon.length) {
      M.toast({html: 'Enter all fields'})
    } else {
      addCategory({
        name: categoryName.current.value,
        icon: categoryIcon
      })
      setCategoryIcon('');
      categoryName.current.value = '';
    }
  };

  return (
    <div className="CategoryBuilder">
      <div id="category-builder-modal" className="modal">
        <form onSubmit={onSubmit}>
        <div className="modal-content">
            <div className="input-field">
              <input ref={categoryName} placeholder="Category Name" type="text" name="name" required />
              <label className="label-icon" htmlFor="name"/>
            </div>
            <div className="input-field">
              <IconSelect iconSelected={onIconSelected}/>
            </div>
        </div>
        <div className="modal-footer">
          <input type="submit" className="modal-close btn blue"/>
          <a href="#!" className="modal-close btn gray">Close</a>
        </div>
        </form>
      </div>
    </div>
  );
};

CategoryBuilder.propTypes = {
  addCategory: PropTypes.func.isRequired,
};

export default connect(null, {addCategory})(CategoryBuilder);
