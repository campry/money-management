import React, {useEffect} from 'react';
import M from 'materialize-css/dist/js/materialize.min.js';

const AddBtn = () => {
  return (
    <a href="#category-builder-modal" className="btn-floating btn-large blue darken-2 modal-trigger">
      <i className="large material-icons">add</i>
    </a>
  );
};

export default AddBtn;
