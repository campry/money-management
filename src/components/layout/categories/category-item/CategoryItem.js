import React, {useEffect, useState} from 'react';
import NumberFormat from 'react-number-format';
import './CategoryItem.scss'

const CategoryItem = ({category}) => {
  const [hovered, setHovered] = useState(false);

  const {
    name,
    spend,
    icon
  } = category;

  return (
    <div className="CategoryItemCircle">
      <div className="row">
        <div className="col s12">
          <div className="card blue-grey darken-1">
            <div className="card-content white-text" onMouseLeave={() => setHovered(false)} onMouseOver={() => setHovered(true)}>
              <span className="card-title">{name}</span>
              <div className="card-icon">
                {hovered
                  ? (<i className="material-icons">add</i>)
                  : (<i className="material-icons">{icon ? icon : 'category'}</i>)}
              </div>
              <div className="card-value">
                <NumberFormat value={spend || 0} displayType={'text'} thousandSeparator={true} prefix={'₴'} />
              </div>
            </div>
            <div className="card-action">
              <a href="#!">View</a>
              <a className="right edit" href="#!">Edit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CategoryItem;
