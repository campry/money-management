import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getCategories, setLoading} from "../../../actions/categoryAction";
import CategoryItem from './category-item/CategoryItem'
import CategoryBuilder from '../modals/category-builder/CategoryBuilder'
import AddBtn from '../add-btn/AddBtn'
import Preloader from '../preloader/Preloader.js'
import './Categories.scss'

const Categories = ({getCategories, setLoading, cats: {categories, loading}}) => {

  useEffect(() => {
    setLoading();
    getCategories();
    // eslint-disable-next-line
  }, []);

  if (loading || categories === null) {
    return <Preloader/>
  }

  return (
    <div className="Categories">
      <div className="header">
        <h1 className="title">Categories</h1>
        <AddBtn/>
      </div>
      <div className="items">
        {categories.map(cat => <CategoryItem key={cat.id} category={cat}/>)}
      </div>
      <CategoryBuilder/>
    </div>
  );
};

Categories.propTypes = {
  cats: PropTypes.object.isRequired,
  getCategories: PropTypes.func.isRequired
};

const mapToStateProps = state => ({
  cats: state.cats
});

export default connect(mapToStateProps, {getCategories, setLoading})(Categories);
