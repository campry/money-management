import React, {Fragment, useState} from 'react';
import './IconSelect.scss'
import {ICONS_LIST} from '../../../../global/iconsList.js'

const IconSelect = ({iconSelected}) => {

  const [active, setActive] = useState(false);
  const [icon, setIcon] = useState('');

  const onIconSelect = e => {
    const icon = e.target.dataset.icon;
    if (icon) {
      setIcon(icon);
      iconSelected(icon);
      setActive(false);
    }
  };

  const clearIcon = () => {
    setIcon('');
  };

  return (
    <div className="IconSelect">
      <div onClick={() => setActive(!active)} className={`i-s-label ${active ? 'i-s-label--active' : ''}`}>
        <span className="i-s-label__text">{icon ? (<i className="material-icons">{icon}</i>) : 'Select Icon'}</span>
        <i className="i-s-label__icon material-icons">{active ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}</i>
      </div>
      {active && (
        <div className="i-s-body" onClick={onIconSelect}>
          {ICONS_LIST.map((icon, index) => <i key={index} data-icon={icon} className="material-icons">{icon}</i>)}
        </div>
      )}
    </div>
  );
};

export default IconSelect;
