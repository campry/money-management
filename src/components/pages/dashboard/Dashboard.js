import React from 'react';
import Categories from '../../layout/categories/Categories.js'

const Dashboard = () => {
  return (
    <div className="Dashboard container">
      <Categories/>
    </div>
  );
};

export default Dashboard;
