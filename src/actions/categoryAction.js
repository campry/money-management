import {
  SET_LOADING,
  GET_CATEGORIES,
  CATEGORY_ERROR, ADD_CATEGORY
} from "./types";
import axios from 'axios'

export const getCategories = () => async dispatch => {
  try {
    const res = await axios.get('/categories');
    dispatch({
      type: GET_CATEGORIES,
      payload: res.data
    })
  } catch (e) {
    console.log(e.response.statusText);
    dispatch({
      type: CATEGORY_ERROR,
      payload: e.response
    })
  }
};

export const addCategory = category => async dispatch => {
  try {
    const res = await axios.post('/categories', category);
    dispatch({
      type: ADD_CATEGORY,
      payload: res.data
    })
  } catch (e) {
    dispatch({
      type: CATEGORY_ERROR,
      payload: e.response
    })
  }
};

export const setLoading = () => {
  return {
    type: SET_LOADING
  }
};
