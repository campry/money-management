import React, {useEffect} from 'react';

import 'materialize-css/dist/css/materialize.min.css';
import M from 'materialize-css/dist/js/materialize.min.js';
import './App.css';

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import {Provider} from 'react-redux'
import store from './store'

import NavBar from './components/layout/navbar/NavBar'
import Dashboard from './components/pages/dashboard/Dashboard'
import NotFound from './components/pages/not-found/NotFound'

const App = () => {

  useEffect(() => {
    M.AutoInit();
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <NavBar/>
          <Switch>
            <Route exact path="/" component={Dashboard}/>
            <Route component={NotFound}/>
          </Switch>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
